import React from 'react';
import PlayingPanel from "./components/PlayingPanel";
import ControlPanel from "./components/ControlPanel";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: 'answer',
            show: true
        };
        this.createRandomString = this.createRandomString.bind(this);
        this.showResult = this.showResult.bind(this);
    }

    createRandomString() {
        let result = '';
        for (let i = 0; i < 4; i++) {
            const ranNum = Math.ceil(Math.random() * 25);
            result += (String.fromCharCode(65 + ranNum));
        }
        this.setState({
                answer: result
            },
            () => {
                setTimeout(() => this.setState({
                    show: false
                }), 3000)
            });
        console.log(result);
    }

    showResult() {
        this.setState({
            show: true
        })
    }

    render() {
        return (
            <div>
                <PlayingPanel answer={this.state.answer}/>
                <ControlPanel showResult={this.showResult} createRandomString={this.createRandomString} answer={this.state.answer}
                              show={this.state.show}/>
            </div>
        )
    }
}

export default App;
import React, {Fragment} from 'react';
import './ControlPanel.less'

class ControlPanel extends React.Component {

    render() {
        return (
            <div className='control-panel'>
                <button className='button-control' onClick={this.props.createRandomString}>New Card</button>
                <div>{this.props.show ? <span>{this.props.answer}</span> : <Fragment/>}</div>
                <div className='button-control'>
                <button onClick={this.props.showResult}>Show Result</button>
                </div>
            </div>
        )
    }
}

export default ControlPanel;
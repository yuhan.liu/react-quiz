import React from 'react';
import './PlayingPanel.less'

class PlayingPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            input: 'your input',
            isCorrect: 'FAILED'
        });
        this.getInput = this.getInput.bind(this);
        this.judgeCorrect = this.judgeCorrect.bind(this);
    }

    getInput(key, value) {
        this.setState({
            [key]: value
        })
    }

    judgeCorrect(event) {
        event.preventDefault();
        const MyInput = this.state.input;
        console.log(MyInput);
        if (MyInput === this.props.answer) {
            this.setState({
                isCorrect: 'SUCCESS'
            })
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.judgeCorrect}>
                    <div className='your-result'>
                        <p>Your Result</p>
                        <p>{this.state.input}</p>
                        <span>{this.state.isCorrect}</span>
                    </div>
                    <div className='guess'>
                    <input type='text' onChange={event => this.getInput('input', event.target.value)}/>
                    <button className='input'>Guess</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default PlayingPanel;